# Definir imagem base
FROM node:10-alpine

# Copiar os arquivos para dentro do container
ADD . /src

# Definir diretório onde os comandos serão executados
WORKDIR /src

# Incluir dependencias globais
RUN npm install -g typescript pm2

# Instalar as dependências do projeto
RUN npm install

# Compilar o projeto com node
RUN npm run build

# Iniciar o projeto
CMD npm start

